;; global parameters and constants

(local lowrez64 (require :lib/lowrez64))

;; window properties & initialization
(local CONF {:title "Minimal LÖVE"
             :identity :minimal-love
             :width 640
             :height 640
              :icon (love.image.newImageData "assets/img/fennel-logo.png")
             :flags {:resizable true :vsync false :minwidth 256 :minheight 256}
             :scalex nil
             :scaley nil})

;; global love2d variables FASTPIG, ha
(local lf love.filesystem)
(local la love.audio)
(local ls love.sound)
(local lt love.thread)
(local lp love.physics)
(local li love.image)
(local lg love.graphics)

(local glyphs " +-/='_(),.:?[]abcdefghijklmnopqrstuvwxyz0123456789")
(local font-3x3 (lg.newImageFont :assets/fnt/3x3.png glyphs 1))

(local debug (lg.newText font-3x3 ""))
(local hello (lg.newText font-3x3 ""))
(hello:set "minimal love")

(fn love.conf [t]
  (t.console false)
  (t.version :11.5)
)

(fn love.load []
  (love.window.setTitle CONF.title)
  (lf.setIdentity CONF.identity)
  (love.window.setMode CONF.width CONF.height CONF.flags)
  (love.window.setIcon CONF.icon)
  (lg.setDefaultFilter :nearest :nearest 0)
  (lg.setFont font-3x3)
  (lowrez64.setup 64))


(fn handle-keyboard-input []
  (when (love.keyboard.isDown :escape)
    (love.event.quit)))

(fn love.update [dt]
  (local fps (love.timer.getFPS))
  (debug:set (.. fps " fps"))
  (handle-keyboard-input))  

(fn love.draw []
  (lowrez64.start)
  (lg.clear 0.5 0 0)
  (lg.setColor 255 255 255)
  (lg.draw debug 0 0)
  (lg.draw hello 0 61)
  (lowrez64.finish))

(fn love.resize [w h]
  (lowrez64.resize w h))



